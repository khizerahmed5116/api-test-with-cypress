/// <reference types="cypress" />
describe('Create and verify Multisms', ()=>{
    const token = Cypress.config('multismsToken');
    const url = Cypress.config('multismsUrl');
    var id = 0;
    it("Post Multisms", ()=>{
        cy.request({
            method: "POST",
            url: url,
            auth: {
                "bearer": token
            },
            body:   
                {
                    "from": "testingsms",
                    "sms": [
                        {
                            "to": "413121231155",
                            "message": "test01"
                        }
                    ]
                }
        }).should((response) => {
            expect(response.status).equal(201)
            expect(response.body).to.have.all.keys('status','id','updated_at', 'created_at', 'size')
            id = response.body.id
        })
    });

    it("Get Multisms", ()=>{
        cy.request({
            method: "GET",
            url: url + id,
            auth: {
                "bearer": token
            }
        }).should((response) => {
            expect(response.status).equal(200)
            expect(response.body).to.have.all.keys('id', 'status', 'size', 'cost', "created_at", "updated_at", "messages")
            expect(response.body.id).eq(id)
            expect(response.body.size).eq(1)
        })
    });
});
