/// <reference types="cypress" />
describe('GET Payment', ()=>{
    const token = Cypress.config('paymentToken');
    const url = Cypress.config('paymentUrl');
    const id = 4;
    it("Get all payments", ()=>{
        cy.request({
            method: "GET",
            url: url + 'payments',
            auth: {
                "bearer": token
            }
        }).should((response) => {
            expect(response.status).equal(200)
            expect(response.body).to.have.property('data')
                Cypress._.each(response.body.data, (response) => {
                    expect(response).to.have.all.keys('id', 'reference', 'amount', 'status')
                })
        })
    });


    it("Get a specfic payment", ()=>{
        cy.request({
            method: "GET",
            url: url + 'payments/' + id,
            auth: {
                "bearer": token
            }
        }).then((response) => {
            cy.log(JSON.stringify(response.body))
            expect(response.status).equal(200)
            expect(response.body).to.have.property('data')
            expect(response.body.data).to.have.all.keys('id', 'reference', 'amount', 'status', "created_at", "updated_at", "provider_id", "provider_status")
        })
    });
});
